package servlet;

import model.User;
import service.UserService;
import service.UserServiceImpl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/show-users.jsp")
public class ShowUsersServlet extends HttpServlet {

  private UserService userService;

  public ShowUsersServlet() throws IOException, SQLException {
    userService = new UserServiceImpl();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    List<User> users = userService.getUsers();
    req.setAttribute("users", users);
    getServletContext().getRequestDispatcher("/show_users.jsp").forward(req, resp);
  }
}
