package servlet;

import dto.UserDto;
import jms.AppMessageConsumer;
import jms.AppMessageProducer;
import service.UserService;
import service.UserServiceImpl;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/add-user.jsp")
public class AddUserServlet extends HttpServlet {

  private UserService userService;
  private AppMessageProducer producer;

  public AddUserServlet() throws IOException, SQLException {
    userService = new UserServiceImpl();
    try {
      new AppMessageConsumer(userService);
      producer = new AppMessageProducer();
    } catch (JMSException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String userName = req.getParameter("username");
    String email = req.getParameter("email");

    try {
      producer.sendMessage(new UserDto(userName, email));
    } catch (JMSException e) {
      throw new RuntimeException(e);
    }
    getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
  }
}
