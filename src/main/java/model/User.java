package model;

import util.Column;

public class User {

  private Long id;
  @Column(name = "social_id")
  private String socialId;
  @Column(name = "user_name")
  private String userName;
  private String photo;
  private String email;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSocialId() {
    return socialId;
  }

  public void setSocialId(String socialId) {
    this.socialId = socialId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", socialId='" + socialId + '\'' +
        ", userName='" + userName + '\'' +
        ", photo='" + photo + '\'' +
        ", email='" + email + '\'' +
        '}';
  }
}
