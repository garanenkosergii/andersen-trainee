package service;

import model.User;

import java.util.List;

public interface UserService {

  void addUser(String userName, String email);

  List<User> getUsers();
}
