package service;

import model.User;
import repository.UserRepository;
import repository.UserRepositoryImpl;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class UserServiceImpl implements UserService {

  private UserRepository userRepository;

  public UserServiceImpl() throws IOException, SQLException {
    userRepository = new UserRepositoryImpl();
  }

  @Override
  public void addUser(String userName, String email) {
    User user = new User();
    user.setUserName(userName);
    user.setEmail(email);
    user.setSocialId(LocalDateTime.now().toString());
    userRepository.save(user);
  }

  public List<User> getUsers() {
    return userRepository.findAll();
  }
}
