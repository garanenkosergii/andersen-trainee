package dto;

import java.io.Serializable;

public class UserDto implements Serializable {

  private String username;
  private String email;

  public UserDto(String username, String email) {
    this.username = username;
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public String getEmail() {
    return email;
  }
}
