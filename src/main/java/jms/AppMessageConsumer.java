package jms;

import dto.UserDto;
import service.UserService;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

public class AppMessageConsumer implements MessageListener {

  private UserService userService;

  public AppMessageConsumer(UserService userService) throws JMSException {
    ConnectionFactory connectionFactory = JmsProvider.getConnectionFactory();
    Connection connection = connectionFactory.createConnection();
    connection.start();

    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    Queue queue = session.createQueue("app-queue");
    MessageConsumer consumer = session.createConsumer(queue);
    consumer.setMessageListener(this);
    this.userService = userService;
  }

  @Override
  public void onMessage(Message message) {
    if (message instanceof ObjectMessage) {
      try {
        Serializable object = ((ObjectMessage) message).getObject();
        if (object.getClass() == UserDto.class) {
          UserDto userDto = (UserDto) object;
          userService.addUser(userDto.getUsername(), userDto.getEmail());
        }
      } catch (JMSException e) {
        throw new RuntimeException(e);
      }
    }
  }

}
