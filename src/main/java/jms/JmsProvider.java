package jms;

import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Arrays;

import javax.jms.ConnectionFactory;

public class JmsProvider {

  public static ConnectionFactory getConnectionFactory() {
    ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(
        "vm://localhost");
    activeMQConnectionFactory.setTrustedPackages(Arrays.asList("dto"));
    return activeMQConnectionFactory;
  }
}