package jms;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

public class AppMessageProducer {

  private Connection connection;
  private Session session;
  private MessageProducer producer;

  public AppMessageProducer() throws JMSException {
    ConnectionFactory connectionFactory = JmsProvider.getConnectionFactory();
    this.connection = connectionFactory.createConnection();
    connection.start();
    this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    Queue queue = session.createQueue("app-queue");
    this.producer = session.createProducer(queue);
  }

  public void sendMessage(Serializable msg) throws JMSException {
    ObjectMessage textMessage = session.createObjectMessage(msg);
    producer.send(textMessage);
  }
}
