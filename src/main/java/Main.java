import model.User;
import repository.UserRepository;
import repository.UserRepositoryImpl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Main {

  public static void main(String[] args) throws SQLException, IOException {

    UserRepository userRepository = new UserRepositoryImpl();

    User user = new User();
    user.setEmail("wert@rt.com");
    user.setPhoto("photo-name.jpg");
    user.setSocialId("1344567");
    user.setUserName("John Dow");

    System.out.println("saving...");
    userRepository.save(user);
    System.out.println(user);

    System.out.println("get all");
    List<User> all = userRepository.findAll();
    System.out.println(all);

    System.out.println("update user name");
    user.setUserName("Morgan");
    userRepository.update(user);

    System.out.println("get all");
    all = userRepository.findAll();
    System.out.println(all);
//
    System.out.println("find by id");
    User byId = userRepository.findById(user.getId());
    System.out.println(byId);

    System.out.println("delete by id");
    userRepository.delete(user.getId());

    System.out.println("get all");
    List<User> all2 = userRepository.findAll();
    System.out.println(all2);
  }

}
