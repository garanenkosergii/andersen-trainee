package repository;

import model.User;
import util.AppDbConnection;
import util.EntityCreateUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

  private Connection conn = AppDbConnection.getConnection();
  private PreparedStatement selectByIdstatement;
  private PreparedStatement selectAllStatement;

  private final String selectByIdStatementTemplate = "SELECT * FROM user WHERE id=?";
  private final String selectAllStatementTemplate = "SELECT * FROM user";

  public UserRepositoryImpl() throws IOException, SQLException {
    selectByIdstatement = conn.prepareStatement(
        selectByIdStatementTemplate,
        ResultSet.TYPE_FORWARD_ONLY,
        ResultSet.CONCUR_UPDATABLE);
    selectAllStatement = conn.prepareStatement(selectAllStatementTemplate);
  }

  @Override
  public void save(User user) {
    try {
      selectByIdstatement.setLong(1, -1);
      ResultSet resultSet = selectByIdstatement.executeQuery();
      resultSet.moveToInsertRow();
      EntityCreateUtil.updateRecordSet(resultSet, user);
      resultSet.insertRow();
      resultSet.next();
      long id = resultSet.getLong("id");
      user.setId(id);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<User> findAll() {
    System.out.println("FIND ALL");
    try {
      List users = new ArrayList();
      try (ResultSet resultSet = selectAllStatement.executeQuery()) {
        while (resultSet.next()) {
          User user = EntityCreateUtil.initEntity(resultSet, User.class);
          users.add(user);
        }
      }
      return users;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public User findById(Long userId) {
    try {
      selectByIdstatement.setLong(1, userId);
      ResultSet resultSet = selectByIdstatement.executeQuery();
      return resultSet.next() ? EntityCreateUtil.initEntity(resultSet, User.class) : null;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void update(User user) {
    try {
      selectByIdstatement.setLong(1, user.getId());
      ResultSet resultSet = selectByIdstatement.executeQuery();
      if (resultSet.next()) {
        EntityCreateUtil.updateRecordSet(resultSet, user);
        resultSet.updateRow();
      } else {
        throw new RuntimeException("user is not found");
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void delete(Long userId) {
    try {
      selectByIdstatement.setLong(1, userId);
      ResultSet resultSet = selectByIdstatement.executeQuery();
      if (resultSet.next()) {
        resultSet.deleteRow();
      } else {
        throw new RuntimeException("user is not found");
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}
