package repository;

import model.User;

import java.util.List;

public interface UserRepository {

  void save(User user);

  List<User> findAll();

  User findById(Long userId);

  void update(User user);

  void delete(Long userId);
}
