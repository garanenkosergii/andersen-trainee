package util;

import model.User;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class EntityCreateUtil {

  public static <T> T initEntity(ResultSet resultSet, Class<T> clazz) {
    try {
      T entity = clazz.newInstance();
      Field[] fields = clazz.getDeclaredFields();
      AccessibleObject.setAccessible(fields, true);
      for (Field field : fields) {
        Column annotation = field.getAnnotation(Column.class);
        String columnName = annotation != null ? annotation.name() : field.getName();
        Class<?> type = field.getType();
        Object columnValue = resultSet.getObject(columnName);
        Object value = convert(columnValue, type);
        field.set(entity, value);
      }
      return entity;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static <T> void updateRecordSet(ResultSet resultSet, T entity) {
    try {
      Field[] fields = User.class.getDeclaredFields();
      AccessibleObject.setAccessible(fields, true);
      for (Field field : fields) {
        Object value = field.get(entity);
        if (value != null) {
          Column annotation = field.getAnnotation(Column.class);
          String columnName = annotation != null ? annotation.name() : field.getName();
          resultSet.updateObject(columnName, value);
        }
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private static <T> T convert(Object obj, Class<T> clazz) {
    if (clazz == LocalDateTime.class && obj.getClass() == Timestamp.class) {
      return (T) ((Timestamp) obj).toLocalDateTime();
    }
    return (T) obj;
  }
}
