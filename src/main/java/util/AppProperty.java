package util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;


public class AppProperty {

  private static Properties properties;

  static {
    Properties properties1 = properties = new Properties();
    URL resource = AppProperty.class.getClassLoader().getResource("database.properties");
//    Path path = Paths.get("database.properties").toAbsolutePath();
//    try (InputStream inputStream = Files.newInputStream(path)) {
    try (InputStream inputStream = resource.openStream()) {
      properties.load(inputStream);
    } catch (IOException ex) {
      ex.printStackTrace();
      throw new RuntimeException(ex);
    }
  }

  public static String getProperty(String prop) {
    return properties.getProperty(prop);
  }
}
