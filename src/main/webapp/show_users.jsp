<%--
  Created by IntelliJ IDEA.
  User: Sergii
  Date: 21.10.2019
  Time: 17:36
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show users</title>
</head>
<body>


<c:forEach var="user" items="${users}">
    <ul>
        <li>
            <span>User name: ${user.userName}</span>
            <span>email: ${user.email}</span>
            <span>social ID: ${user.socialId}</span>
        </li>
    </ul>
</c:forEach>


</body>
</html>
