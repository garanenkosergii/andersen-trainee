<%--
  Created by IntelliJ IDEA.
  User: Sergii
  Date: 21.10.2019
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add user</title>
</head>
<body>
<form action="add-user.jsp" method="post">
    <p>
        <label for="username">User name:</label> <input name="username" id="username"
                                                        placeholder="User name"/>
    </p>
    <p>
        <label for="email">User email:</label><input name="email" id="email"
                                                     placeholder="User email"/>
    </p>
    <input type="submit" value="Save"/>
</form>
</body>
</html>
