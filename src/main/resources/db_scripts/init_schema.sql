CREATE TABLE user
(
  id        BIGINT AUTO_INCREMENT
    PRIMARY KEY,
  social_id VARCHAR(255) NULL,
  user_name VARCHAR(255) NULL,
  photo     VARCHAR(255) NULL,
  email     VARCHAR(255) NULL,
  CONSTRAINT user_social_id_uindex
  UNIQUE (social_id)
);
CREATE TABLE post
(
  id      BIGINT AUTO_INCREMENT
    PRIMARY KEY,
  content TEXT                         NULL,
  user_id BIGINT                       NULL,
  created DATETIME                     NULL,
  visible BIT DEFAULT b'1'             NOT NULL,
  CONSTRAINT fk_post_user
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE INDEX fk_post_user
  ON post (user_id);